@extends('layouts.app')

@section('content')
    <form action="{{route("datos.update",$datos->id)}}" method="POST">
        {{method_field('PATCH')}}
        @csrf
        <div class="container">

            <div class="form-group">
                <input type="text" class="form-control" name="nombre" value="{{$datos->nombre}}" placeholder="Nombre">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="apellidop" value="{{$datos->apellidop}}" placeholder="Apellido Paterno">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="apellidom" value="{{$datos->apellidom}}" placeholder="Apellido Materno">
            </div>

            <div class="form-group">
                <input type="date" class="form-control" name="fechadenacimiento" value="{{$datos->fechadenacimiento}}" placeholder="Fecha de Nacimiento">
            </div>

            <button type="submit" class="btn btn-primary">Guardar</button>

        </div>
    </form>
@endsection

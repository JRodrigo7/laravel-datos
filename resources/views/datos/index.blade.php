@extends('layouts.app')

@section('content')
    <div class="text-center">
        <h1>Datos Personales</h1>
    </div>
    <div class="container">
        <table class="table table-bordered">
            <thead class="thead-dark">
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>Fecha de Nacimiento</th>
                <th>Acciones</th>
            </tr>

            </thead>
            <tbody>
            @foreach($datos as $datos)
            <tr>
                <td>{{$datos->id}}</td>
                <td>{{$datos->nombre}}</td>
                <td>{{$datos->apellidop}}</td>
                <td>{{$datos->apellidom}}</td>
                <td>{{$datos->fechadenacimiento}}</td>
                <td>
                    <a href="{{url('/datos/'.$datos->id.'/edit')}}" class="btn btn-link">Editar</a>
                    @include('datos.delete',['$datos'=>$datos])
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        <a href="{{url('/datos/create')}}" class="btn btn-primary">Agregar</a>
    </div>

@endsection

<form action="{{url('/datos/'.$datos->id)}}" method="POST" class="d-inline-block">
    @csrf
    {{method_field('DELETE')}}

    <input type="submit" class="btn btn-link" style="color: red" value="Eliminar">

</form>

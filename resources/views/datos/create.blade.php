@extends('layouts.app')

@section('content')
    <form action="{{route("datos.store")}}" method="POST">
        @csrf
        <div class="container">

            <div class="form-group">
                <input type="text" class="form-control" name="nombre" placeholder="Nombre">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="apellidop" placeholder="Apellido Paterno">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="apellidom" placeholder="Apellido Materno">
            </div>

            <div class="form-group">
                <input type="date" class="form-control" name="fechadenacimiento" placeholder="Fecha de Nacimiento">
            </div>

            <button type="submit" class="btn btn-primary">Guardar</button>

        </div>
    </form>
@endsection

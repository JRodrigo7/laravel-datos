<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Datos;
use Illuminate\Support\Facades\Auth;

class DatosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos = Datos::all();
        return view('datos.index',['datos'=>$datos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('datos.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datos = new Datos();
        $datos->nombre = $request->nombre;
        $datos->apellidop = $request->apellidop;
        $datos->apellidom = $request->apellidom;
        $datos->fechadenacimiento = $request->fechadenacimiento;
        $datos->user_id = Auth::user()->id;

        if($datos->save())
        {
            return redirect("/datos");
        }
        else
        {
            return view('datos.create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $datos = Datos::find($id);
        return view('datos.edit',["datos"=>$datos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datos = Datos::find($id);
        $datos->nombre = $request->nombre;
        $datos->apellidop = $request->apellidop;
        $datos->apellidom = $request->apellidom;
        $datos->fechadenacimiento = $request->fechadenacimiento;
        if($datos->save())
        {
            return redirect("/datos");
        }
        else
        {
            return view('datos.edit',['datos'=>$datos]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Datos::destroy($id);
        return redirect("/datos");
    }
}
